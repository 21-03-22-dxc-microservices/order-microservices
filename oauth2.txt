redirect_uri - http://localhost:8555/oauth2/callback

Client-id: 0oa4dy9mx8jo2ILAj5d7
Client-secret: wEPNpEiSiEvXnjyamxjm8Z0LypLYtbevscOyrzYx

For end users:
  Authorization endpoint: https://dev-7858070.okta.com/oauth2/default/v1/authorize

For client applications:
  Token endpoint: https://dev-7858070.okta.com/oauth2/default/v1/token

Authorization link:
Authorization request: https://dev-7858070.okta.com/oauth2/default/v1/authorize?client_id=0oa4dy9mx8jo2ILAj5d7&response_type=code&scope=openid&redirect_uri=http://localhost:8555/oauth2/callback&state=state-296bc9a0-a2a2-4a57-be1a-d0e2fd9bb601

Authorization response: http://localhost:8555/oauth2/callback?code=6eCAsQuUiA6kner1bTuUO3DDWKY0SS_Zfq-ua8ju6_U&state=state-296bc9a0-a2a2-4a57-be1a-d0e2fd9bb601

Authorization code: 6eCAsQuUiA6kner1bTuUO3DDWKY0SS_Zfq-ua8ju6_U

Token endpont:
curl --request POST \
  --url https://dev-7858070.okta.com/oauth2/default/v1/token \
  --header 'accept: application/json' \
  --header 'authorization: Basic MG9hY...' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data 'grant_type=authorization_code&redirect_uri=http%3A%2F%2Flocalhost%3A8080&code=6eCAsQuUiA6kner1bTuUO3DDWKY0SS_Zfq-ua8ju6_U'



