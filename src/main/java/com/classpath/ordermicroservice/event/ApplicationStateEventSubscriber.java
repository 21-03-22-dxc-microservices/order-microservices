package com.classpath.ordermicroservice.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.LivenessState;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
@Slf4j
public class ApplicationStateEventSubscriber {
    @EventListener
    public void subscribeLivenessState(AvailabilityChangeEvent<LivenessState> state){
      log.error(" Exception caused :: " + state.toString());
    }
}
