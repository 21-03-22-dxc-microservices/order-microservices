package com.classpath.ordermicroservice.event;

import com.classpath.ordermicroservice.model.LineItem;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import java.util.stream.IntStream;
import java.util.stream.Stream;

@Configuration
@Slf4j
public class BootstrapAppData {



    private final OrderRepository orderRepository;
    private Faker faker = new Faker();

    public BootstrapAppData(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void handleApplicationReadyEvent(ApplicationReadyEvent event){
        IntStream.range(0, 101).forEach(index -> {
            Order order = Order
                            .builder()
                                .email(faker.internet().emailAddress())
                                .price(faker.number().randomDouble(2, 5000, 15000))
                                .name(faker.name().firstName())
                            .build();

            IntStream.range(0, faker.number().numberBetween(2,5)).forEach(val -> {
                LineItem lineItem = LineItem
                                        .builder()
                                            .qty(faker.number().numberBetween(2,5))
                                            .price(faker.number().randomDouble(2, 300, 2000))
                                            .name(faker.commerce().productName())
                                        .build();
                order.addLineItem(lineItem);
                this.orderRepository.save(order);
            });

        });
        log.info("Application is initialized ....");
    }
}
