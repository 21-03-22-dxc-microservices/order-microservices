package com.classpath.ordermicroservice.event;

import com.classpath.ordermicroservice.model.Order;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import java.time.LocalDateTime;

@RequiredArgsConstructor
@Getter
public class OrderEvent {
    private final Order order;
    private final LocalDateTime timeStamp;
    private final OrderStatus orderStatus;


    public enum OrderStatus {
        ORDER_ACCEPTED,
        ORDER_FULFILLED,
        ORDER_REJECTED
    }
}


