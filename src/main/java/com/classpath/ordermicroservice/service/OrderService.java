package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.event.OrderEvent;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Set;

import static com.classpath.ordermicroservice.event.OrderEvent.OrderStatus.ORDER_ACCEPTED;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;
    //private final RestTemplate restTemplate;
    private final Source source;


    //@CircuitBreaker(name = "inventoryservice", fallbackMethod = "fallback")
   // @Retry(name = "retryService", fallbackMethod = "fallback")
    @Transactional
    public Order saveOrder(Order order){
        log.info("About to update the inventory :: ");
        //this.restTemplate.postForEntity("http://localhost:9222/api/v1/inventory", null, Integer.class );
        Order savedOrder = this.orderRepository.save(order);
        OrderEvent orderAcceptedEvent = new OrderEvent(order, LocalDateTime.now(), ORDER_ACCEPTED);
        //push the message to the broker
        Message<OrderEvent> orderEventMessage = MessageBuilder.withPayload(orderAcceptedEvent).build();
        this.source.output().send(orderEventMessage);
        return savedOrder;
    }

    private Order fallback(Throwable exception){
       log.error("Exception while communicationg with inventory microservice :: {}", exception.getMessage());
       return Order.builder().build();
    }

    public Set<Order> fetchAllOrders(){
        return Set.copyOf(this.orderRepository.findAll());
    }

    public Order findOrderById(long id){
        return this.orderRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid order id"));
    }

    public Order updateOrder(long id, Order order){
        this.orderRepository
                        .findById(id)
                        .ifPresent(fetchedOrder -> this.orderRepository.save(order));
        return order;
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }
}
